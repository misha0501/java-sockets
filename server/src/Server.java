import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class Server {
    public static HashMap<String, ClientThread> clients;
    public static HashMap<String, Group>
            groups;

    public static void main(String[] args) {
        int SERVER_PORT = 1337;
        clients = new HashMap<>();
        groups = new HashMap<>();

        try {
            // the socket used by the server
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);

            // infinite loop to wait for connections ( till server is active )
            while (true) {
                Utils.display("Server waiting for Clients on port " + SERVER_PORT + ".");

                // accept connection if requested from client
                Socket socket = serverSocket.accept();
                Utils.display("Client connected ....");

                DataInputStream sInput = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
                DataOutputStream sOutput = new DataOutputStream(socket.getOutputStream());


                System.out.println(sInput.toString());
                ClientThread clientThread = new ClientThread(socket, sOutput, sInput);
                clientThread.start();

            }

        } catch (IOException e) {
            e.printStackTrace();
            Utils.display(" Exception on new ServerSocket: " + e.getMessage());
        }
    }
}
