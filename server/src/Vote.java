import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

enum UserVote {
    YES,
    NO,
    EMPTY
}

enum VoteResult {
    YES,
    NO,
    ENDED,
    NO_RESULT
}

public class Vote {
    private final HashMap<String, UserVote> participants = new HashMap<>();
    private final Date voteStart;

    /**
     * @param participants hash set with participants ids
     */
    public Vote (HashSet<String> participants) {
        for (String i : participants) {
            this.participants.put(i, UserVote.EMPTY);
        }
        voteStart = new Date();
    }

    /**
     * @param id user id
     * @param vote vote (yes or no)
     */
    public void vote (String id, UserVote vote) {
        participants.put(id, vote);
    }

    /**
     * If more than 1 minutes is passed we say that it is ended. If no, count yes and no and give result depends on values.
     * @return vote result
     */
    public VoteResult getVoteResult () {
        long diff = new Date().getTime() - voteStart.getTime();
        if (diff > (long) 60 * 1000) {
            return VoteResult.ENDED;
        }
        int yes = 0;
        int no = 0;
        for (Map.Entry<String, UserVote> entry : participants.entrySet()) {
            UserVote vote = entry.getValue();

            if (vote == UserVote.YES) {
                yes++;
            }
            if (vote == UserVote.NO) {
                no++;
            }
        }

        if (yes > participants.size() / 2) {
            return VoteResult.YES;
        }
        if (no > participants.size() / 2) {
            return VoteResult.NO;
        }
        return VoteResult.NO_RESULT;
    }
}
