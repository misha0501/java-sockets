import java.util.UUID;

public class Utils {
    /**
     * Shorter way of using System.out.println
     * @param msg message that we want to log
     */
    public static void display(String msg) {
        System.out.println(msg);
    }

    /**
     * @param message message that all clients will receive
     */
    // to broadcast a message to all Clients
    public static synchronized boolean broadcast(String message) {
        // to check if message is private i.e. client to client message
        String[] w = message.split(" ", 3);

        boolean isPrivate = false;
        if (w[1].charAt(0) == '@')
            isPrivate = true;


        // if private message, send message to mentioned username only
        if (isPrivate) {
            String usernameToCheck = w[1].substring(1, w[1].length());

            message = w[0] + w[2];
            String messageLf = message + "\n";
            boolean found = false;
            // we loop in reverse order to find the mentioned username
            for (ClientThread clientThread : Server.clients.values()) {
                String clientUserName = clientThread.getUsername();
                if (clientUserName.equals(usernameToCheck)) {
                    // try to write to the Client if it fails remove it from the list
                    if (!clientThread.writeMsg(messageLf)) {
                        Server.clients.remove(clientThread.id);

                        display("Disconnected Client " + clientThread.username + " removed from list.");
                    }
                    // username found and delivered the message
                    found = true;
                    break;
                }
            }

            // mentioned user not found, return false
            return found;
        }
        // if message is a broadcast message
        else {
            String messageLf = message + "\n";
            // display message
            System.out.print(messageLf);

            // we loop in reverse order in case we would have to remove a Client
            // because it has disconnected
            for (ClientThread clientThread : Server.clients.values()) {
                if (!clientThread.writeMsg(messageLf)) {
                    Server.clients.remove(clientThread.id);
                    display("Disconnected Client " + clientThread.username + " removed from list.");
                }
            }

        }
        return true;
    }

    /**
     * @return randomly generated uuid
     */
    static String generateId() {
        return UUID.randomUUID().toString();
    }
}
