import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.*;

// One instance of this thread will run for each client
class ClientThread extends Thread {
    // the socket to get messages from client
    Socket socket;
    DataInputStream sInput;
    DataOutputStream sOutput;
    // my unique id (easier for deconnection)
    String id;
    // the Username of the Client
    String username;

    PrintWriter writer;

    boolean lastPongReceived;

    Thread currentThread;

    // Constructor
    ClientThread(Socket socket, DataOutputStream sOutput, DataInputStream sInput) {
        // a unique id
        this.id = Utils.generateId();
        this.socket = socket;
        this.sOutput = sOutput;
        this.sInput = sInput;
        this.lastPongReceived = true;
        this.currentThread = this;
    }

    /**
     * @return client username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username client username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @param type char of message type
     * @param data message data
     */
    public void sendMessage(char type, String data) {
        try {
            byte[] dataInBytes = data.getBytes(StandardCharsets.UTF_8);

            this.sOutput.writeChar(type);
            this.sOutput.writeInt(dataInBytes.length);
            this.sOutput.write(dataInBytes);
            writer.print(this.sOutput);
        } catch (Exception e) {

        }
    }

    /**
     * @param type char of message type
     * @param command message data
     * @param dataBytes data bytes
     * @throws IOException
     */
    private void sendMessage(char type, String command, byte[] dataBytes) throws IOException {
        this.sOutput.writeChar(type);
        this.sOutput.writeInt(dataBytes.length);
        this.sOutput.writeUTF(command);
        this.sOutput.write(dataBytes);
        writer.print(this.sOutput);
    }


    /**
     * Run client thread and liten for new messages
     */
    // infinite loop to read and forward message
    public void run() {
        Thread thread = new Thread(() -> {
            try {
                runPing();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();

        try {
            this.sOutput = sOutput;
            this.sInput = sInput;
            writer = new PrintWriter(this.sOutput);

            char dataType;
            try {
                while (!socket.isClosed() && socket.isConnected() && (dataType = sInput.readChar()) != 0) {
                    int length = sInput.readInt();

                    if (dataType == 'b') {
                        System.out.println("dataType B received ");

                        byte[] dataBytes = new byte[length];

                        // checking if there is any commands (UTF)
//                    if (length >= sInput.available()) {
//                        this.sendMessage('s', "400");
//                        break;
//                    }

                        String msg = sInput.readUTF();
                        System.out.println(msg);
                        String[] requestArray = msg.split(" ", 2);
                        String command = requestArray[0];
                        String request = "";

                        if (requestArray.length > 1) {
                            request = requestArray[1];
                        }

                        // reads data
                        sInput.readFully(dataBytes);

                        switch (command) {
                            case "PM_S":
                                if (request.isEmpty()) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                System.out.println("Received an encrypted message for: " + request);
                                boolean encryptedMessageSent = false;
                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null && client.username.equals(request)) {
                                        client.sendMessage('b', "PM_S " + this.username, dataBytes);
                                        System.out.println("Sending an encrypted message to " + request);
                                        encryptedMessageSent = true;
                                    }
                                }
                                if (!encryptedMessageSent) {
                                    System.out.println("User: " + request + " wasn't found.");
                                    this.sendMessage('s', "404 " + " user wasn't found");
                                    break;
                                }
                                this.sendMessage('s', "200");
                                break;
                        }
                    } else if (dataType == 's') {
                        byte[] messageByte = new byte[length];
                        boolean end = false;
                        StringBuilder dataString = new StringBuilder(length);
                        int totalBytesRead = 0;
                        while (!end) {
                            int currentBytesRead = sInput.read(messageByte);
                            totalBytesRead = currentBytesRead + totalBytesRead;
                            if (totalBytesRead <= length) {
                                dataString
                                        .append(new String(messageByte, 0, currentBytesRead, StandardCharsets.UTF_8));
                            } else {
                                dataString
                                        .append(new String(messageByte, 0, length - totalBytesRead + currentBytesRead,
                                                StandardCharsets.UTF_8));
                            }
                            if (dataString.length() >= length) {
                                end = true;
                            }
                        }

                        String[] requestArray = dataString.toString().split(" ", 3);
                        String command = requestArray[0];
                        String request = "";
                        String request2 = "";

                        if (requestArray.length > 1) {
                            request = requestArray[1];
                        }
                        if (requestArray.length > 2) {
                            request2 = requestArray[2];
                        }

                        switch (command) {
                            case "GET_UUID":
                                boolean found = false;
                                String uuid = UUID.randomUUID().toString();
                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null && client.username.equals(request)) {
                                        found = true;
                                    }
                                }

                                if (!found) {
                                    this.sendMessage('s', "404");
                                    break;
                                }

                                this.sendMessage('s', "UUID " + uuid);
                                break;
                            case "FILE_SEND":
                                System.out.println("command " + command);
                                String[] requestArray2 = dataString.toString().split(" ", 4);
                                System.out.println(dataString.toString());
                                System.out.println(Arrays.toString(requestArray2));

                                String fileReceiver = requestArray2[1];
                                String fileName = requestArray2[2];
                                String uuid1 = requestArray2[3];

                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null && client.username.equals(fileReceiver)) {
                                        client.sendMessage('s', "FILE_SEND " + fileName + " " + uuid1 + " " + this.username);
                                        break;
                                    }
                                }
                                break;
                            case "CONN":
                                boolean foundUser = false;
                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null && client.username.equals(request)) {
                                        foundUser = true;
                                        break;
                                    }
                                }
                                if (this.username != null) {
                                    sendMessage('s', "409");
                                    break;
                                }
                                if (foundUser) {
                                    writer.println();
                                    sendMessage('s', "409");
                                    Utils.display("SERVER: User  " + username + " already logged in");
                                    break;
                                }
                                this.setUsername(request);
                                Server.clients.put(this.id, this);
                                sendMessage('s', "200 " + username);
                                Utils.display("SERVER: User  " + username + " logged in");

                                break;
                            case "LOGOUT":
                                Server.clients.remove(id);
                                break;
                            case "BCST":
                                System.out.println("Receive a message to broadcast: " + request);

                                for (ClientThread client : Server.clients.values()) {
                                    this.sendMessage('s', "200");
                                    if (client.username != null && client != this)
                                        client.sendMessage('s', "BCST " + this.username + " " + request);
                                }
                                System.out.println("BCST message: " + request);
                                break;
                            case "LIST":
                                ArrayList<String> clients = new ArrayList<>();
                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null) {
                                        clients.add(client.username);
                                    }
                                }
                                String clientsStringArray = String.join(", ", clients);
                                System.out.println(clientsStringArray);

                                sendMessage('s', "LIST " + clientsStringArray);
                                break;
                            case "PM":
                                System.out.println("Receive a message to send to: " + request);
                                if (request2.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                boolean messageSent = false;
                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null && client.username.equals(request)) {
                                        client.sendMessage('s', "PM " + request + " " + request2);
                                        System.out.println("Sending PM to " + client.username);
                                        messageSent = true;
                                    }
                                }
                                if (!messageSent) {
                                    System.out.println("User: " + request + " wasn't found.");
                                    this.sendMessage('s', "404 " + " user wasn't found");
                                    break;
                                }
                                this.sendMessage('s', "200");


                                break;
                            case "GROUP_CREATE":
                                if (request.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                if (Server.groups.get(request) != null) {
                                    this.sendMessage('s', "409");
                                    break;
                                }
                                Group group = new Group(request);
                                group.addMember(this.username);
                                Server.groups.put(request, group);
                                this.sendMessage('s', "201");
                                break;
                            case "GROUPS":
                                ArrayList<String> groups = new ArrayList<>();
                                for (Group g : Server.groups.values()) {
                                    groups.add(g.getName());
                                }
                                String groupsStringArray = String.join(", ", groups);
                                System.out.println(groupsStringArray);

                                sendMessage('s', "GROUPS " + groupsStringArray);
                                break;
                            case "JOIN_GROUP":
                                if (request.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                if (Server.groups.get(request) == null) {
                                    this.sendMessage('s', "404");
                                    break;
                                }
                                Server.groups.get(request).addMember(this.username);
                                this.sendMessage('s', "200");
                                break;
                            case "PM_GROUP":
                                System.out.println(dataString.toString());
                                if (request.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                if (Server.groups.get(request) == null) {
                                    this.sendMessage('s', "404");
                                    break;
                                }
                                if (!Server.groups.get(request).memberExists(this.username.equals("") ? this.id : this.username)) {
                                    this.sendMessage('s', "405");
                                    break;
                                }

                                for (ClientThread client : Server.clients.values()) {
                                    if (Server.groups.get(request).memberExists(client.username) && !client.username.equals(this.username)) {
                                        client.sendMessage('s', "PM_GROUP " + request + " " + this.username + " " + request2);
                                    }
                                }
                                this.sendMessage('s', "200");
                                break;
                            case "LEAVE_GROUP":
                                if (request.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                if (Server.groups.get(request) == null) {
                                    this.sendMessage('s', "404");
                                    break;
                                }

                                Server.groups.get(request).removeMember(this.username);
                                this.sendMessage('s', "200");
                                break;
                            case "LIST_GROUP":
                                System.out.println(dataString.toString());
                                if (request.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                if (Server.groups.get(request) == null) {
                                    this.sendMessage('s', "404");
                                    break;
                                }
                                if (!Server.groups.get(request).memberExists(this.username.equals("") ? this.id : this.username)) {
                                    this.sendMessage('s', "405");
                                    break;
                                }

                                ArrayList<String> groupLids = new ArrayList<>();

                                for (ClientThread client : Server.clients.values()) {
                                    if (Server.groups.get(request).memberExists(client.username) && client.username != null) {
                                        groupLids.add(client.getUsername());
                                    }
                                }

                                String groupLidsStringArray = String.join(", ", groupLids);
                                System.out.println(groupLidsStringArray);

                                sendMessage('s', "LIST_GROUP " + groupLidsStringArray);
                                break;
                            case "POLL_KICK_PERSON":
                                Group groupWhereKick = Server.groups.get(request2);

                                System.out.println(request);
                                System.out.println(request2);
                                System.out.println(this.username);

                                if (groupWhereKick == null) {
                                    this.sendMessage('s', "404");
                                    break;
                                }

                                if (!groupWhereKick.memberExists(this.username)) {
                                    this.sendMessage('s', "401");
                                    break;
                                }

                                if (groupWhereKick.isVoteRunning()) {
                                    this.sendMessage('s', "409");
                                    break;
                                }

                                groupWhereKick.startVote(request);
                                break;
                            case "VOTE_KICK_PERSON":
                                System.out.println(dataString.toString());
                                Group groupWhereVote = Server.groups.get(request);
                                if (groupWhereVote == null) {
                                    this.sendMessage('s', "404");
                                    break;
                                }

                                if (!groupWhereVote.memberExists(this.username)) {
                                    this.sendMessage('s', "401");
                                    break;
                                }

                                if (!groupWhereVote.isVoteRunning()) {
                                    this.sendMessage('s', "409");
                                    break;
                                }

                                UserVote vote = UserVote.valueOf(request2);
                                groupWhereVote.vote(this.username, vote);
                                break;
                            case "PONG":
                                lastPongReceived = true;
                                break;
                            case "PKEY_REQ":
                                if (request.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                System.out.println("Received a message to get public key of : " + request);
                                boolean publicKeyRequested = false;
                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null && client.username.equals(request)) {
                                        client.sendMessage('s', "PKEY_REQ " + this.username);
                                        System.out.println("Sending a public key request to " + client.username);
                                        publicKeyRequested = true;
                                    }
                                }
                                if (!publicKeyRequested) {
                                    System.out.println("User: " + request + " wasn't found.");
                                    this.sendMessage('s', "404 " + " user wasn't found");
                                    break;
                                }
                                this.sendMessage('s', "200");
                                break;
                            case "PKEY_RES":
                                if (request.equals("") || request2.equals("")) {
                                    this.sendMessage('s', "400");
                                    break;
                                }
                                System.out.println("Received a message to send public key of : " + this.username + " , to: " + request);
                                boolean publicKeySent = false;
                                for (ClientThread client : Server.clients.values()) {
                                    if (client.username != null && client.username.equals(request)) {
                                        client.sendMessage('s', "PKEY_OF " + this.username + " " + request2);
                                        System.out.println("Sending a public key of " + client.username);
                                        publicKeySent = true;
                                    }
                                }
                                if (!publicKeySent) {
                                    System.out.println("User: " + request + " wasn't found.");
                                    this.sendMessage('s', "404 " + " user wasn't found");
                                    break;
                                }
                                this.sendMessage('s', "200");
                                break;
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Send ping commands to user and disconnect him if we don't receive pong
     */
    private void runPing() throws IOException {
        // And From your main() method or any other method
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    if (!lastPongReceived) {
                        socket.close();
                        if (username != null) {
                            Server.clients.remove(username);
                        }
                        t.cancel();
                        currentThread.stop();

                        return;
                    }
                    lastPongReceived = false;

                    if (username != null) {
                        Utils.display("send PING to " + username);
                    } else {
                        Utils.display("send PING to " + id);
                    }

                    sendMessage('s', "PING");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 3000);


    }

    // close everything
    private void close() {
        try {
            if (sOutput != null) sOutput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (sInput != null) sInput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ;
        try {
            if (socket != null) socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // write a String to the Client output stream
    public boolean writeMsg(String msg) {
        // if Client is still connected send the message to it
        if (!socket.isConnected()) {
            close();
            return false;
        }
        // write the message to the stream
        try {
            sOutput.writeUTF(msg);
        }
        // if an error occurs, do not abort just inform the user
        catch (IOException e) {
            e.printStackTrace();
            Utils.display("Error sending message to " + username);
            Utils.display(e.toString());
        }
        return true;
    }
}
