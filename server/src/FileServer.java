import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class FileServer {
    public static HashMap<String, String> files = new HashMap<>();

    public static void main(String[] args) {
        int SERVER_PORT = 1338;
        try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Connection established");

                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

                String uuid = null;
                String filename = null;
                char dataType;
                try {
                    while (!socket.isClosed() && socket.isConnected() && (dataType = dataInputStream.readChar()) != 0) {
                        if (dataType == 's') {
                            int length = dataInputStream.readInt();
                            byte[] messageByte = new byte[length];
                            boolean end = false;
                            StringBuilder dataString = new StringBuilder(length);
                            int totalBytesRead = 0;
                            while (!end) {
                                int currentBytesRead = dataInputStream.read(messageByte);
                                totalBytesRead = currentBytesRead + totalBytesRead;
                                if (totalBytesRead <= length) {
                                    dataString
                                            .append(new String(messageByte, 0, currentBytesRead, StandardCharsets.UTF_8));
                                } else {
                                    dataString
                                            .append(new String(messageByte, 0, length - totalBytesRead + currentBytesRead,
                                                    StandardCharsets.UTF_8));
                                }
                                if (dataString.length() >= length) {
                                    end = true;
                                }
                            }

                            String[] requestArray = dataString.toString().split(" ", 2);
                            uuid = requestArray[0];
                            filename = requestArray[1];
                        }

                        if (uuid != null) {
                            // Upload file to the server
                            if (dataType == 'f') {
                                System.out.println("Received file....");
                                files.put(uuid, filename);
                                receiveFile(filename, dataInputStream);
                            }

                            // Send file to the user
                            if (dataType == 'r') {
                                System.out.println("Received a request to download a file" );
                                sendFile(uuid, dataOutputStream);
                            }
                        }
                    }
                } catch (Exception e) {}
            }
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    /**
     * @param fileName name of the file
     * @param dataInputStream
     * Receive file from a user and save it as a file name
     */
    private static void receiveFile(String fileName, DataInputStream dataInputStream) throws Exception {
        int bytes = 0;
        FileOutputStream fileOutputStream = new FileOutputStream(fileName);

        long size = dataInputStream.readLong();
        byte[] buffer = new byte[4 * 1024];
        while (size > 0 && (bytes = dataInputStream.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
            fileOutputStream.write(buffer, 0, bytes);
            size -= bytes;
        }
        fileOutputStream.close();
    }

    /**
     * @param uuid uuid of the clients to get file name
     * @param dataOutputStream
     * Send a file to the user by uuid
     */
    private static void sendFile(String uuid, DataOutputStream dataOutputStream) throws Exception {
        String path = files.get(uuid);
        int bytes = 0;
        File file = new File(path);
        FileInputStream fileInputStream = new FileInputStream(file);

        dataOutputStream.writeLong(file.length());
        byte[] buffer = new byte[4 * 1024];
        while ((bytes = fileInputStream.read(buffer)) != -1) {
            dataOutputStream.write(buffer, 0, bytes);
            dataOutputStream.flush();
//            System.out.println("flush");
        }
//        System.out.println("end");
        fileInputStream.close();
    }
}
