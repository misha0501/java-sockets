import java.io.IOException;
import java.util.HashSet;

public class Group {
    private final String name;
    private final HashSet<String> members;
    private Vote currentVote;
    private String voteUsername;

    /**
     * @param name name of the group
     */
    public Group (String name) {
        this.name = name;
        members = new HashSet<>();
    }

    public boolean isVoteRunning () {
        return this.currentVote != null;
    }

    /**
     * @param voteUsername username who will be kicked if vote passes
     */
    public void startVote (String voteUsername) throws InterruptedException, IOException {
        if (currentVote != null) {
            return;
        }

        this.voteUsername = voteUsername;
        currentVote = new Vote(members);

        for (ClientThread client : Server.clients.values()) {
            if (memberExists(client.username)) {
                client.sendMessage('s', "GROUP_START_POLL " + name + " " + voteUsername);
            }
        }
        Thread thread = new Thread(() -> {
            try {
                VoteResult result = VoteResult.NO_RESULT;
                while (result == VoteResult.NO_RESULT) {
                    result = getVoteResult();
                    Thread.sleep(1000);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    /**
     * @param id user id
     * @param vote vote enum (yes or no)
     */
    public void vote (String id, UserVote vote) throws IOException {
        if (currentVote == null) {
            return;
        }
        currentVote.vote(id, vote);

        for (ClientThread client : Server.clients.values()) {
            if (memberExists(client.username)) {
                client.sendMessage('s', "GROUP_VOTED " + name + " " + id + " " + vote.toString());
            }
        }
    }

    public HashSet<String> getMembers() {
        return members;
    }

    /**
     * Returns vote result and kicks a user if vote result is yes.
     * @return vote result
     */
    public VoteResult getVoteResult () throws IOException {
        if (currentVote == null) {
            return null;
        }
        VoteResult result = currentVote.getVoteResult();
        if (result == VoteResult.YES) {
            System.out.println(voteUsername);
            for (ClientThread client : Server.clients.values()) {
                System.out.println(client.username);
                System.out.println(memberExists(client.username));
                if (client.username.equals(voteUsername)) {
                    System.out.println("KICKED");
                    client.sendMessage('s', "GROUP_KICKED " + name);
                }
            }
            for (ClientThread client : Server.clients.values()) {
                if (memberExists(client.username)) {
                    client.sendMessage('s', "GROUP_KICKED_PERSON " + name + " " + voteUsername );
                }
            }
            members.remove(voteUsername);
        }

        if (result != VoteResult.NO_RESULT) {
            this.currentVote = null;
        }
        return result;
    }

    /**
     * @param id id of the member who will be added
     */
    public void addMember (String id) {
        members.add(id);
    }

    /**
     * @param id  id of the member who will be removed
     */
    public void removeMember (String id) {
        members.remove(id);
    }

    /**
     * @param id id of the member who will be checked
     * @return whether member is in a group
     */
    public boolean memberExists (String id) {
        return members.contains(id);
    }

    /**
     * @return group name
     */
    public String getName() {
        return this.name;
    }
}
