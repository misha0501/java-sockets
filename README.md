# Initiate voting
Possibility to kick a user by means of voting.

## Happy flow
```
KICKUSER <groupname> <username>
S: 200 Vote kick recieved correctly.
```
Other clients in the group receive the following message: 
```
S: KICKUSER <groupname> <username>
```

## Error messages
* No user
```
S: 404 User <username> does not exist.
```
* No group
```
S: 404 Group <groupname> does not exist.
```
* Not a member of group
```
S: 403 You are not a member of the group <groupname>.
```
* No concurrent session
```
S: 409 There is already a voting session going on.
```

## Voting
When a user whishes to vote on the kick:
```
😄 KICKREPSONSE <groupname> <username> <y/n>
```

## Error messages
* No user
```
S: 404 User <username> does not exist.
```
* No group
```
S: 404 Group <groupname> does not exist.
```
* Not a member of group
```
S: 403 You are not a member of the group <groupname>.
```
* No voting session for that user at the moment.
```
S: 409 No voting session for <username> at the moment.
```

# README
This is the level 1 implementation of the Server for the module internet technologie.

To run the Server open the command line terminal and type:
```
$ node sever.js
```

The connection to the Server can be tested using netcat. Open a new command line terminal (while the Server is running) and type:
```
$ nc 127.0.0.1 1337
INFO Welcome to the Server 1.0
CONN ALice
200 ALice
QUIT
200 Goodbye
```

## Parameters
The Server.js has two parameters that can be changed for development purposes:
* PORT: the port on which the Server listens (default port 1337 is used)
* SHOULD_PING: a boolean which determines whether the Server should send a PING periodically (default is true). When starting the development of the client it is useful to set it to false in order to keep the connection open.
