import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.UUID;

public class Client {
    public static DataOutputStream dataOutputStream;
    public static DataOutputStream dataOutputStreamFile;
    public static OutputStream outputFile;

    public static PrintWriter writer;
    public static PrintWriter writerFile;

    public static Scanner scanner;

    public static String outputMessage = "";
    public static String randomGroupName = generateGroupName();
    public static String randomUserName = generateUsername();
    public static HashMap<String, String> encryptedMessages;
    public static Encryptor encryptor;
    public static String uuid = null;
    public static OutputStream output;
    public static int lastCommand = -1;
    public static String filePath;
    public static String fileReceiverUsername;
    public static String fileName;

    public static void log(String message) {
        System.out.println(message);
    }

    public static void showMenu() throws IOException {
        log("------------------------");
        log(outputMessage);
        log("------------------------");
        log("0 - Login with [ " + randomUserName + " ]  create group [ " + randomGroupName + " ] ");
        log("1 - Login to the server");
        log("2 - Show list of connected users");
        log("3 - Broadcast message");
        log("4 - Send private message");
        log("5 - Get list of groups");
        log("6 - Create group");
        log("7 - Join group");
        log("8 - Send group message");
        log("9 - Leave group");
        log("10 - Show list of group members");
        log("11 - Kick user from group");
        log("12 - Vote for kick");
        log("13 - Send encrypted private message");
        log("14 - Send file to another user");
        log("------------------------");

        scanner = new Scanner(System.in);
        int input = 0;
        try {
            input = scanner.nextInt();
        } catch (InputMismatchException e) {
            showMenu();
            return;
        }

        lastCommand = input;

        switch (input) {
            case 0 -> {
                sendMessage("CONN " + randomUserName);
                sendMessage("GROUP_CREATE " + randomGroupName);
            }
            case 1 -> {
                System.out.println("Your username: ");
                scanner = new Scanner(System.in);
                String username = scanner.nextLine();
                sendMessage("CONN " + username);
            }
            case 2 -> sendMessage("LIST");
            case 3 -> {
                System.out.println("Your message: ");
                scanner = new Scanner(System.in);
                String message = scanner.nextLine();
                sendMessage("BCST " + message);
                log("Sending message: " + message);
            }
            case 4 -> {
                System.out.println("Username: ");
                scanner = new Scanner(System.in);
                String pmUsername = scanner.nextLine();
                System.out.println("Your message: ");
                String pmMessage = scanner.nextLine();
                sendMessage("PM " + pmUsername + " " + pmMessage);
                log("Sending private message: " + pmMessage);
            }
            case 5 -> sendMessage("GROUPS");
            case 6 -> {
                System.out.println("Group name: ");
                scanner = new Scanner(System.in);
                String groupName = scanner.nextLine();
                sendMessage("GROUP_CREATE " + groupName);
            }
            case 7 -> {
                System.out.println("Group name: ");
                scanner = new Scanner(System.in);
                String joinGroupName = scanner.nextLine();
                sendMessage("JOIN_GROUP " + joinGroupName);
            }
            case 8 -> {
                System.out.println("Group name: ");
                scanner = new Scanner(System.in);
                String messageGroupName = scanner.nextLine();
                System.out.println("Your message: ");
                String messageGroup = scanner.nextLine();
                sendMessage("PM_GROUP " + messageGroupName + " " + messageGroup);
            }
            case 9 -> {
                System.out.println("Group name: ");
                scanner = new Scanner(System.in);
                String leaveGroupName = scanner.nextLine();
                sendMessage("LEAVE_GROUP " + leaveGroupName);
            }
            case 10 -> {
                System.out.println("Group name: ");
                scanner = new Scanner(System.in);
                String listGroupName = scanner.nextLine();
                sendMessage("LIST_GROUP " + listGroupName);
            }
            case 11 -> {
                System.out.println("Group name where to kick user: ");
                scanner = new Scanner(System.in);
                String groupWhereKick = scanner.nextLine();
                System.out.println("Enter username: ");
                scanner = new Scanner(System.in);
                String userToKick = scanner.nextLine();
                sendMessage("POLL_KICK_PERSON " + userToKick + " " + groupWhereKick);
            }
            case 12 -> {
                System.out.println("Group name where to vote for kick user: ");
                scanner = new Scanner(System.in);
                String groupWhereVote = scanner.nextLine();
                System.out.println("Enter vote: ");
                System.out.println("1 - YES");
                System.out.println("2 - NO");
                scanner = new Scanner(System.in);
                int voteResult = scanner.nextInt();
                String voteString = "YES";
                if (voteResult == 2) {
                    voteString = "NO";
                }
                sendMessage("VOTE_KICK_PERSON " + groupWhereVote + " " + voteString);
            }
            case 13 -> {
                System.out.println("Username: ");
                scanner = new Scanner(System.in);
                String pmsUsername = scanner.nextLine();
                System.out.println("Your message: ");
                String pmsMessage = scanner.nextLine();
                encryptedMessages.put(pmsUsername, pmsMessage);
                sendMessage("PKEY_REQ " + pmsUsername);
                log("Saving encrypted private message for : " + pmsUsername);
            }
            case 14 -> {
                System.out.println("Username: ");
                scanner = new Scanner(System.in);
                fileReceiverUsername = scanner.nextLine();
                System.out.println("File path: ");
                scanner = new Scanner(System.in);
                filePath = scanner.nextLine();
                sendMessage("GET_UUID " + fileReceiverUsername);
                uuid = null;
            }
            case 15 -> {
                receiveFile();
            }
        }
        showMenu();
    }

    private static void receiveFile() {
        String SERVER_ADDRESS = "localhost";
        int SERVER_PORT = 1338;

        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT)) {
            outputFile = socket.getOutputStream();
            DataInputStream sInput = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

            writerFile = new PrintWriter(outputFile, true);

            dataOutputStreamFile = new DataOutputStream(outputFile);


            System.out.println("Connected to the file server...");
            int bytes = 0;

            String data = uuid + " 1";
            byte[] dataInBytes = data.getBytes(StandardCharsets.UTF_8);
            dataOutputStreamFile.writeChar('s');
            dataOutputStreamFile.writeInt(dataInBytes.length);
            dataOutputStreamFile.write(dataInBytes);
            writerFile.print(dataOutputStreamFile);
            dataOutputStreamFile.writeChar('r');
            writerFile.print(dataOutputStreamFile);

            long size;
            while ((size = sInput.readLong()) != 0) {
                FileOutputStream fileOutputStream = new FileOutputStream(fileName);

                byte[] buffer = new byte[4 * 1024];
                while (size > 0 && (bytes = sInput.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
                    fileOutputStream.write(buffer, 0, bytes);
                    size -= bytes;
                }
                fileOutputStream.close();
            }

            System.out.println("File was received");


        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {}
    }

    private static void sendFile (String fileName, String username) {
        String SERVER_ADDRESS = "localhost";
        int SERVER_PORT = 1338;

        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT)) {
            outputFile = socket.getOutputStream();
            DataInputStream sInput = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

            writerFile = new PrintWriter(outputFile, true);

            dataOutputStreamFile = new DataOutputStream(outputFile);


            System.out.println("Connected to the file server...");
            int bytes = 0;

            File file = new File(fileName);

            long fileLength = file.length();
            System.out.println(fileLength);

            String data = uuid + " " + file.getName();
            byte[] dataInBytes = data.getBytes(StandardCharsets.UTF_8);
            dataOutputStreamFile.writeChar('s');
            dataOutputStreamFile.writeInt(dataInBytes.length);
            dataOutputStreamFile.write(dataInBytes);
            writerFile.print(dataOutputStreamFile);

            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] buffer = new byte[4 * 1024];
            dataOutputStreamFile.writeChar('f');
            while ((bytes = fileInputStream.read(buffer)) != -1) {
                dataOutputStreamFile.writeLong(bytes);
                dataOutputStreamFile.write(buffer, 0, bytes);
                writerFile.print(dataOutputStreamFile);
//                System.out.println("flush");
            }
            fileInputStream.close();

            sendMessage("FILE_SEND " + username + " " + file.getName() + " " + uuid);


        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {}
    }

    public static void main(String[] args) throws Exception {
        encryptedMessages = new HashMap<>();
        encryptor = new Encryptor();
        String SERVER_ADDRESS = "localhost";
        int SERVER_PORT = 1337;

        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT)) {
            output = socket.getOutputStream();
            DataInputStream sInput = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

            writer = new PrintWriter(output, true);

            dataOutputStream = new DataOutputStream(output);

            Thread thread = new Thread(() -> {
                try {
                    Client.showMenu();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            thread.start();

            readMessage(sInput);
        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("No response from the server. You are disconnected");
            System.exit(1);

        }

    }

    public static void readMessage(DataInputStream sInput) throws Exception {
        char dataType;
        while ((dataType = sInput.readChar()) != 0) {

            int length = sInput.readInt();

            // 'b' is used for sending encrypted messages
            if (dataType == 'b') {
                // checking if there is any commands
                if (length >= sInput.available()) {
                    return;
                }

                byte[] dataBytes = new byte[length];

                String[] requestArray = sInput.readUTF().split(" ", 2);
                String command = requestArray[0];

                // reads data
                sInput.readFully(dataBytes);

                switch (command) {
                    case "PM_S":
                        String username = requestArray[1];
                        System.out.println("Received an encrypted message from " + username);
                        handleEncryptedMessage(dataBytes);
                        break;
                }
            }

            // 's' is used for sending all other messages
            if (dataType == 's') {
                byte[] messageByte = new byte[length];
                boolean end = false;
                StringBuilder dataString = new StringBuilder(length);
                int totalBytesRead = 0;
                while (!end) {
                    int currentBytesRead = sInput.read(messageByte);
                    totalBytesRead = currentBytesRead + totalBytesRead;
                    if (totalBytesRead <= length) {
                        dataString
                                .append(new String(messageByte, 0, currentBytesRead, StandardCharsets.UTF_8));
                    } else {
                        dataString
                                .append(new String(messageByte, 0, length - totalBytesRead + currentBytesRead,
                                        StandardCharsets.UTF_8));
                    }
                    if (dataString.length() >= length) {
                        end = true;
                    }
                }

                String[] requestArray = dataString.toString().split(" ", 4);
                String command = requestArray[0];

                // Somebody wants to send you a file
                // Send UUID to a sender
                switch (command) {
                    case "FILE_SEND" -> {
                        String[] requestArrayFileSend = dataString.toString().split(" ", 4);
                        fileName = requestArrayFileSend[1];
                        String uuid1 = requestArrayFileSend[2];
                        String sender = requestArrayFileSend[3];
                        uuid = uuid1;

                        System.out.println(sender + " wants to send you a file: " + fileName + ". Uuid: " + uuid1);
                        System.out.println("Do you want to download it?");
                        System.out.println("15) YES ");

                    }
                    case "GROUP_ALERT" -> {
                        String[] requestArrayAlert = dataString.toString().split(" ", 2);
                        System.out.println("Group alert: " + requestArrayAlert[1]);
                    }
                    case "GROUP_KICKED" -> {
                        String[] requestArrayKicked = dataString.toString().split(" ", 2);
                        System.out.println("You have been kicked from: " + requestArrayKicked[1]);
                    }
                    case "GROUP_START_POLL" -> {
                        String[] requestArrayStartPoll = dataString.toString().split(" ", 3);
                        System.out.println("Group " + requestArrayStartPoll[1] + " starts a poll to kick: " + requestArrayStartPoll[2] + ".");
                    }
                    case "GROUP_KICKED_PERSON" -> {
                        String[] requestArrayKickedPerson = dataString.toString().split(" ", 3);
                        System.out.println("Group " + requestArrayKickedPerson[1] + " message: " + requestArrayKickedPerson[2] + " has been kicked from the group.");
                    }
                    case "GROUP_VOTED" -> {
                        String[] requestArrayPollVoted = dataString.toString().split(" ", 4);
                        System.out.println("Group " + requestArrayPollVoted[1] + ": " + requestArrayPollVoted[2] + " voted for " + requestArrayPollVoted[3]);
                    }
                    case "PING" -> sendMessage("PONG");
                    case "BCST" -> {
                        String[] requestArrayBCST = dataString.toString().split(" ", 3);
                        System.out.println("Broadcast message from " + requestArrayBCST[1] + ". Message: " + requestArrayBCST[2]);
                    }
                    case "PM" -> {
                        String[] requestArrayPM = dataString.toString().split(" ", 3);
                        System.out.println("Private message from " + requestArrayPM[1] + ". Message: " + requestArrayPM[2]);
                    }
                    case "LIST", "LIST_GROUP", "GROUPS" -> {
                        String listOfData = dataString.toString().split(" ", 2)[1];
                        printListOfData(listOfData);
                    }
                    case "PM_GROUP" -> {
                        String[] requestPM_GROUP = dataString.toString().split(" ", 4);
                        System.out.println("Message in group " + requestPM_GROUP[1] + " from " + requestPM_GROUP[2] + " | Message: " + requestPM_GROUP[3]);
                    }
                    case "PKEY_REQ" -> {
                        String[] pKeyRequest = dataString.toString().split(" ", 2);
                        System.out.println("Public key request from " + pKeyRequest[1]);
                        System.out.println("Sending public key.");
                        sendPublicKeyTo(pKeyRequest[1]);
                    }
                    case "PKEY_OF" -> {
                        String[] pKeyOf = dataString.toString().split(" ", 3);
                        System.out.println("Public key received from " + pKeyOf[1] + ". Public key: " + pKeyOf[2]);
                        handlePublicKeyReceived(pKeyOf[1], pKeyOf[2]);
                    }
                    case "FILE_REQUEST" -> {
                        System.out.println("Username wants to send you a file: filename. Accept?");
                        uuid = dataString.toString().split(" ", 2)[1];
                    }
                    case "UUID" -> {
                        uuid = dataString.toString().split(" ", 2)[1];
                        Thread thread = new Thread(() -> {
                            try {
                                sendFile(filePath, fileReceiverUsername);
                            } catch (Exception e) {
                                System.out.println("Error while sending a file");
                                e.printStackTrace();
                            }
                        });
                        thread.start();
                    }
                    default -> handleDefaultServerResponses(command);
                }

            }
        }
    }

    private static void printListOfData(String data) {
        switch (lastCommand) {
            case 2 -> log("Users connected at the moment: " + data);
            case 5 -> log("List of groups: " + data);
            case 10 -> log("List of group members: " + data);
        }
    }

    private static void handleDefaultServerResponses(String command) {
        switch (command) {
            case "200" -> System.out.println("Operation finished successfully.");
            case "201" -> System.out.println("Resource was created successfully.");
            case "400" -> System.out.println("Bad request, you provided wrong data.");
            case "401", "405" -> System.out.println("Operation is not allowed.");
            case "404" -> System.out.println("Something you were looking for wasn't found.");
            case "409" -> System.out.println("Your request is conflicting with current state of the server.");
        }
    }

    private static void handleEncryptedMessage(byte[] msg) throws Exception {
        String decryptedMsg = encryptor.decryptMessage(msg);
        System.out.println("Message: " + decryptedMsg);
    }

    private static void handlePublicKeyReceived(String username, String publicKey) throws Exception {
        if (encryptedMessages.get(username) != null) {
            byte[] encryptedMessageBytes = encryptMessage(encryptedMessages.get(username), publicKey);
            sendMessage("PM_S " + username, encryptedMessageBytes);
            encryptedMessages.remove(username);
            System.out.println("Encrypted message is sent to " + username);
        }
    }

    private static byte[] encryptMessage(String msg, String publicKey) throws Exception {
        return encryptor.encryptMessage(msg, publicKey);
    }

    private static void sendPublicKeyTo(String username) throws IOException {
        String publicKey = encryptor.getPublicKey();
        sendMessage("PKEY_RES " + username + " " + publicKey);
    }

    private static void sendMessage(String data) throws IOException {
        byte[] dataInBytes = data.getBytes(StandardCharsets.UTF_8);
        dataOutputStream.writeChar('s');
        dataOutputStream.writeInt(dataInBytes.length);
        dataOutputStream.write(dataInBytes);
        writer.print(dataOutputStream);
    }

    private static void sendMessage(String command, byte[] dataInBytes) throws IOException {
        dataOutputStream.writeChar('b');
        dataOutputStream.writeInt(dataInBytes.length);
        dataOutputStream.writeUTF(command);
        dataOutputStream.write(dataInBytes);
        writer.print(dataOutputStream);
    }

    public static String generateGroupName() {
        return UUID.randomUUID().toString();
    }

    public static String generateUsername() {
        return UUID.randomUUID().toString();
    }

}
