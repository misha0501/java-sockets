
import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;


public class Encryptor {
    private static final String RSA
            = "RSA";

    private static KeyPair keypair;

    public Encryptor() throws Exception {
        keypair = generateRSAKkeyPair();
    }


    // Generating public & private keys
    // using RSA algorithm.
    private KeyPair generateRSAKkeyPair()
            throws Exception {
        SecureRandom secureRandom
                = new SecureRandom();
        KeyPairGenerator keyPairGenerator
                = KeyPairGenerator.getInstance(RSA);

        keyPairGenerator.initialize(
                2048, secureRandom);
        return keyPairGenerator
                .generateKeyPair();
    }

    // Encryption function which converts
    // the plainText into a cipherText
    // using private Key.
    private byte[] do_RSAEncryption(
            String plainText,
            PublicKey publicKey)
            throws Exception {
        Cipher cipher
                = Cipher.getInstance(RSA);

        cipher.init(
                Cipher.ENCRYPT_MODE, publicKey);

        return cipher.doFinal(
                plainText.getBytes());
    }

    // Decryption function which converts
    // the ciphertext back to the
    // orginal plaintext.
    private String do_RSADecryption(
            byte[] cipherText,
            PrivateKey privateKey)
            throws Exception {
        Cipher cipher
                = Cipher.getInstance(RSA);

        cipher.init(Cipher.DECRYPT_MODE,
                privateKey);

        byte[] result
                = cipher.doFinal(cipherText);

        return new String(result);
    }

    public byte[] encryptMessage(String msg, String publicKey) throws Exception {
        byte[] publicBytes = DatatypeConverter.parseHexBinary(publicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey pubKey = keyFactory.generatePublic(keySpec);


        return do_RSAEncryption(
                msg,
                pubKey);
    }

    public String decryptMessage(byte[] msgBytes) throws Exception {
        return do_RSADecryption(
                msgBytes,
                keypair.getPrivate());

    }

    public String getPublicKey() {
        return DatatypeConverter.printHexBinary(
                keypair.getPublic().getEncoded());
    }

}
